Build:

```
mvn clean package
```

Usage:

```
java -jar target\H2ToJson-1.0.jar -u sa -j jdbc:h2:tcp://localhost/e://qemuRecord -s "SELECT * FROM QEMURECORDER limit 0,2"
```
