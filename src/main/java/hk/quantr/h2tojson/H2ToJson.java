package hk.quantr.h2tojson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hk.quantr.peterswing.PropertyUtil;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class H2ToJson {

	public static void main(String args[]) throws ParseException, SQLException, JsonProcessingException {
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption("h", "help");
		options.addOption(Option.builder("u")
				.required(false)
				.hasArg()
				.argName("username")
				.build());
		options.addOption(Option.builder("p")
				.required(false)
				.hasArg()
				.argName("password")
				.build());

		options.addOption(Option.builder("j")
				.required(true)
				.hasArg()
				.argName("jdbc connection string")
				.build());
		options.addOption(Option.builder("s")
				.required(true)
				.hasArg()
				.argName("sql")
				.build());

		if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar qemu-importer.jar [OPTION] <input file>", options);
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);
			} catch (java.text.ParseException ex) {
				Logger.getLogger(H2ToJson.class.getName()).log(Level.SEVERE, null, ex);
			}
			System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}

		CommandLineParser cliParser = new DefaultParser();
		CommandLine cmd = cliParser.parse(options, args);

		Connection conn = DriverManager.getConnection(cmd.getOptionValue("j"), cmd.getOptionValue("u"), cmd.getOptionValue("p") == null ? "" : cmd.getOptionValue("p"));
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(cmd.getOptionValue("s"));
		ResultSetMetaData metadata = rs.getMetaData();
		int columnCount = metadata.getColumnCount();
		TreeSet<Pair<String, Integer>> cols = new TreeSet<>();
		for (int i = 1; i <= columnCount; i++) {
			cols.add(Pair.of(metadata.getColumnName(i), metadata.getColumnType(i)));
//			System.out.println(metadata.getColumnName(i) + ",");
		}
		ArrayList list = new ArrayList();
		while (rs.next()) {
			HashMap map = new HashMap();
//			//Retrieve by column name
			for (Pair<String, Integer> p : cols) {
				if (p.getRight() == java.sql.Types.INTEGER || p.getRight() == java.sql.Types.SMALLINT || p.getRight() == java.sql.Types.BIGINT) {
					System.out.print(rs.getInt(p.getLeft()));
					map.put(p.getLeft(), rs.getInt(p.getLeft()));
				} else if (p.getRight() == java.sql.Types.VARCHAR) {
					System.out.print(rs.getString(p.getLeft()));
					map.put(p.getLeft(), rs.getString(p.getLeft()));
				} else if (p.getRight() == java.sql.Types.DATE) {
					System.out.print(rs.getDate(p.getLeft()));
					map.put(p.getLeft(), rs.getDate(p.getLeft()));
				} else if (p.getRight() == java.sql.Types.TIMESTAMP) {
					System.out.print(rs.getTimestamp(p.getLeft()));
					map.put(p.getLeft(), rs.getTimestamp(p.getLeft()));
				} else {
					System.out.print("unknow type=" + p.getRight());
				}
				System.out.print(",");
			}
			System.out.println();
			list.add(map);
		}
		rs.close();
		conn.close();

		ObjectMapper objectMapper = new ObjectMapper();
		System.out.println(objectMapper.writeValueAsString(list));
	}
}
